/*
 * Copyright 2018 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define FUSE_USE_VERSION 30

#include <assert.h>
#include <errno.h>
#include <signal.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <deque>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>

#include <fuse_lowlevel.h>
#include <openssl/sha.h>

#include "buildbox.h"

#define MAX_OPEN_INODES 512
#define ENTRY_TIMEOUT (24 * 3600)
#define XATTR_CHECKSUM "user.checksum.sha256"

class CasDirectory;

class CasInode {
public:
	CasInode(CasDirectory *parent);
	virtual ~CasInode() {}

	virtual void flush();
	void update_mtime();
	void set_modified();
	bool is_modified();

	static CasInode *from_fuse(fuse_ino_t inode_id);

	/* Only set for parent-child links in the input tree
	 * as it's not needed for newly created links and can't
	 * handle multiple hardlinks. */
	CasDirectory *parent;

	struct stat attr;
	Digest digest;

protected:
	bool modified; /* modified/created */
	std::string temppath; /* only for modified/created files */
};

class CasDirectory : public CasInode {
public:
	CasDirectory();
	CasDirectory(CasDirectory *parent, const Digest& digest);
	virtual ~CasDirectory() {}

	virtual void flush();

	static CasDirectory *open_from_fuse(fuse_ino_t inode_id);

	std::map<std::string, CasInode *> dentries;

private:
	bool dentries_populated;
};

class CasFile : public CasInode {
public:
	CasFile();
	CasFile(CasDirectory *parent, const Digest& digest, bool is_executable);
	virtual ~CasFile() {}

	void copy_on_write();
	void close();

	static CasFile *open_from_fuse(fuse_ino_t inode_id);

	int fd;
};

class CasSymlink : public CasInode {
public:
	CasSymlink(const std::string& target);
	CasSymlink(CasDirectory *parent, const std::string& target);
	virtual ~CasSymlink() {}

	static CasSymlink *open_from_fuse(fuse_ino_t inode_id);

	std::string target;
};

struct fs_opts {
	const char *local_path;
	const char *input_digest;
	const char *output_digest;
	const char *chdir;
	const char *mountpoint;

	const char *remote_url;
	const char *server_cert;
	const char *client_key;
	const char *client_cert;
	int prefetch;
};

class CasFS {
public:
	int mktemp(std::string& temppath);
	int fd_from_digest(const Digest& digest);

	void batch_download_complete();
	void prefetch_batch();
	void prefetch();

	struct fs_opts opts;
	int local_dfd;
	CasDirectory *root_inode;

	std::deque<CasFile *> open_files;

	uid_t uid;
	gid_t gid;

	struct fuse_session *fuse;

	std::vector<const char *> env_argv;

	pid_t cmd_child_pid;
	int cmd_child_status;

	Client client;

	std::deque<Digest> prefetch_deque;
	std::deque<Digest> prefetch_next_deque;
};

static CasFS fs;

/* State for FUSE directory opened by userspace process */
class OpenDirectory {
public:
	OpenDirectory(CasDirectory *inode);

	std::map<std::string, CasInode *>::iterator iterator;
	long offset;
};

static std::string objdir(const Digest& digest)
{
	std::string path("objects/");
	path.append(digest.hash().substr(0, 2));
	return path;
}

static std::string objpath(const Digest& digest)
{
	std::string path(objdir(digest));
	path.append("/");
	path.append(digest.hash().substr(2));
	return path;
}

int CasFS::mktemp(std::string& temppath)
{
	/* work around missing mkstempat() */
	temppath = fs.opts.local_path;
	temppath.append("/tmp/XXXXXX");
	int fd = mkstemp(&temppath[0]);
	if (fd < 0) {
		throw std::system_error(errno, std::generic_category());
	}
	return fd;
}

int CasFS::fd_from_digest(const Digest& digest)
{
	int fd = openat(this->local_dfd, objpath(digest).c_str(), O_RDONLY);
	if (fd >= 0) {
		return fd;
	}

	if (errno != ENOENT) {
		/* I/O error */
		throw std::system_error(errno, std::generic_category());
	}

	/* File not in local cache, download it from the CAS server */

	if (!this->opts.remote_url) {
		throw std::runtime_error("No CAS server configured");
	}

	/* Download it to temporary location */
	std::string temppath;
	fd = this->mktemp(temppath);
	try {
		this->client.download(fd, digest);

		/* Move it to final location */
		mkdirat(this->local_dfd, objdir(digest).c_str(), 0755);
		if (renameat(AT_FDCWD, temppath.c_str(), this->local_dfd, objpath(digest).c_str()) < 0) {
			throw std::system_error(errno, std::generic_category());
		}
	} catch (std::exception& e) {
		unlink(temppath.c_str());
		::close(fd);
		throw;
	}

	lseek(fd, 0, SEEK_SET);
	return fd;
}

void CasFS::batch_download_complete()
{
	const Digest *digest;
	const std::string *data;

	while (this->client.batch_download_next(&digest, &data)) {
		if (digest->size_bytes() != (int64_t) data->size()) {
			throw std::runtime_error("Download failed: digest and data size mismatch");
		}

		std::string temppath;
		int fd = this->mktemp(temppath);
		try {
			ssize_t ret = write(fd, data->c_str(), data->size());
			if (ret < 0) {
				throw std::system_error(errno, std::generic_category());
			} else if (ret != digest->size_bytes()) {
				throw std::runtime_error("Download failed: short write");
			}

			/* Move it to final location */
			mkdirat(this->local_dfd, objdir(*digest).c_str(), 0755);
			if (renameat(AT_FDCWD, temppath.c_str(), this->local_dfd, objpath(*digest).c_str()) < 0) {
				throw std::system_error(errno, std::generic_category());
			}
		} catch (std::exception& e) {
			::close(fd);
			unlink(temppath.c_str());
			throw;
		}
	}
}

void CasFS::prefetch_batch()
{
	this->batch_download_complete();

	/* All previously scheduled directories are now locally available,
	 * move them to the processing queue. */
	while (fs.prefetch_next_deque.size() > 0) {
		fs.prefetch_deque.push_back(fs.prefetch_next_deque.front());
		fs.prefetch_next_deque.pop_front();
	}
}

void CasFS::prefetch()
{
	fs.prefetch_deque.push_back(fs.root_inode->digest);

	while (fs.prefetch_deque.size() + fs.prefetch_next_deque.size() > 0) {
		if (fs.prefetch_deque.size() == 0) {
			this->prefetch_batch();
		}

		Digest digest = fs.prefetch_deque.front();
		fs.prefetch_deque.pop_front();

		struct stat st;
		int fd = fs.fd_from_digest(digest);

		Directory directory;
		if (!directory.ParseFromFileDescriptor(fd)) {
			::close(fd);
			throw std::runtime_error("Failed to parse directory object");
		}
		::close(fd);
		fd = -1;

		for (int i = 0; i < directory.directories_size(); i++) {
			auto child_digest = directory.directories(i).digest();
			if (fstatat(this->local_dfd, objpath(child_digest).c_str(), &st, 0) == 0) {
				/* Skip download, already in local cache.
				 * Add directory to processing queue. */
				fs.prefetch_deque.push_back(child_digest);
				continue;
			}
			if (child_digest.size_bytes() >= fs.client.max_batch_total_size_bytes) {
				/* Too large for batch request, download in independent request. */
				::close(fs.fd_from_digest(child_digest));
				/* Add directory to processing queue. */
				fs.prefetch_deque.push_back(child_digest);
			} else {
				if (!fs.client.batch_download_add(child_digest)) {
					/* Not enough space left in batch request.
					 * Complete pending batch first. */
					this->prefetch_batch();
					fs.client.batch_download_add(child_digest);
				}
				/* Directory will be available after completing pending batch.
				 * Add directory to deferred processing queue. */
				fs.prefetch_next_deque.push_back(child_digest);
			}
		}

		for (int i = 0; i < directory.files_size(); i++) {
			auto child_digest = directory.files(i).digest();
			if (fstatat(this->local_dfd, objpath(child_digest).c_str(), &st, 0) == 0) {
				/* Skip download, already in local cache. */
				continue;
			}
			if (child_digest.size_bytes() >= fs.client.max_batch_total_size_bytes) {
				/* Too large for batch request, download in independent request. */
				::close(fs.fd_from_digest(child_digest));
			} else {
				if (!fs.client.batch_download_add(child_digest)) {
					/* Not enough space left in batch request.
					 * Complete pending batch first. */
					this->prefetch_batch();
					fs.client.batch_download_add(child_digest);
				}
			}
		}
	}

	/* Fetch final batch. */
	this->prefetch_batch();
}

CasInode *CasInode::from_fuse(fuse_ino_t inode_id)
{
	if (inode_id == FUSE_ROOT_ID) {
		return fs.root_inode;
	} else {
		return reinterpret_cast<CasInode *>(inode_id);
	}
}

CasInode::CasInode(CasDirectory *parent) : attr()
{
	this->parent = parent;

	this->attr.st_ino = reinterpret_cast<fuse_ino_t>(this);

	this->attr.st_uid = fs.uid;
	this->attr.st_gid = fs.gid;
	this->attr.st_nlink = 1;

	this->modified = false;
}

CasDirectory::CasDirectory() : CasInode(nullptr)
{
	/* empty directory */

	this->attr.st_mode = S_IFDIR | 0755;

	this->dentries_populated = true;
	this->update_mtime();
	this->set_modified();
}

CasDirectory::CasDirectory(CasDirectory *parent, const Digest& digest) : CasInode(parent)
{
	this->attr.st_mode = S_IFDIR | 0755;

	this->digest = digest;
	this->dentries_populated = false;
}

CasFile::CasFile() : CasInode(nullptr)
{
	/* empty file */

	this->attr.st_mode = S_IFREG | 0644;

	int fd = fs.mktemp(this->temppath);
	if (fd < 0) {
		throw std::system_error(errno, std::generic_category());
	}
	::close(fd);

	this->fd = -1;
	this->update_mtime();
	this->set_modified();
}

CasFile::CasFile(CasDirectory *parent, const Digest& digest, bool is_executable) : CasInode(parent)
{
	this->attr.st_mode = S_IFREG | 0644;
	this->attr.st_size = digest.size_bytes();

	if (is_executable) {
		this->attr.st_mode |= 0111;
	}

	this->digest = digest;
	this->fd = -1;
}

CasSymlink::CasSymlink(const std::string& target) : CasInode(nullptr)
{
	this->attr.st_mode = S_IFLNK | 0777;

	this->target = target;

	this->set_modified();
}

CasSymlink::CasSymlink(CasDirectory *parent, const std::string& target) : CasInode(parent)
{
	this->attr.st_mode = S_IFLNK | 0777;

	this->target = target;
}

CasDirectory *CasDirectory::open_from_fuse(fuse_ino_t inode_id)
{
	CasInode *inode = CasInode::from_fuse(inode_id);
	if (!S_ISDIR(inode->attr.st_mode)) {
		throw std::runtime_error("Inode is not a directory");
	}

	CasDirectory *dir = (CasDirectory *) inode;

	if (dir->dentries_populated) {
		return dir;
	}

	/* Populate directory entries from CAS directory object */

	int fd = fs.fd_from_digest(inode->digest);

	Directory directory;
	if (!directory.ParseFromFileDescriptor(fd)) {
		::close(fd);
		throw std::runtime_error("Failed to parse directory object");
	}
	::close(fd);

	for (int i = 0; i < directory.files_size(); i++) {
		const FileNode& file_node = directory.files(i);
		dir->dentries[file_node.name()] = new CasFile(dir, file_node.digest(), file_node.is_executable());
	}

	for (int i = 0; i < directory.directories_size(); i++) {
		const DirectoryNode& directory_node = directory.directories(i);
		dir->dentries[directory_node.name()] = new CasDirectory(dir, directory_node.digest());
	}

	for (int i = 0; i < directory.symlinks_size(); i++) {
		const SymlinkNode& symlink_node = directory.symlinks(i);
		dir->dentries[symlink_node.name()] = new CasSymlink(dir, symlink_node.target());
	}

	dir->dentries_populated = true;

	return dir;
}

CasFile *CasFile::open_from_fuse(fuse_ino_t inode_id)
{
	CasInode *inode = CasInode::from_fuse(inode_id);
	if (!S_ISREG(inode->attr.st_mode)) {
		throw std::runtime_error("Inode is not a regular file");
	}

	CasFile *file = (CasFile *) inode;

	if (file->fd >= 0) {
		/* already open */
		return file;
	}

	if (fs.open_files.size() >= MAX_OPEN_INODES) {
		CasFile *open_file = fs.open_files.back();
		open_file->close();
		fs.open_files.pop_back();
	}

	if (file->modified) {
		file->fd = open(file->temppath.c_str(), O_RDWR);
		if (file->fd < 0) {
			throw std::system_error(errno, std::generic_category());
		}
	} else {
		file->fd = fs.fd_from_digest(file->digest);
	}

	fs.open_files.push_front(file);

	return file;
}

void CasFile::close()
{
	assert(this->fd >= 0);

	::close(this->fd);
	this->fd = -1;
}

void CasInode::set_modified()
{
	if (!this->modified) {
		if (this->parent) {
			this->parent->set_modified();
		}
		this->modified = true;
	}
}

bool CasInode::is_modified()
{
	return this->modified;
}

void CasFile::copy_on_write()
{
	if (this->modified) {
		/* new file or already copied */
		return;
	}

	std::string temppath;
	int fd = fs.mktemp(temppath);

	std::vector<char> buffer(BUFFER_SIZE);
	ssize_t count = 0;
	for (off_t offset = 0; offset < this->attr.st_size; offset += count) {
		count = pread(this->fd, &buffer[0], buffer.size(), offset);
		if (count < 0 || write(fd, &buffer[0], count) < 0) {
			::close(fd);
			throw std::system_error(errno, std::generic_category());
		}
	}

	::close(this->fd);
	this->fd = fd;

	this->temppath = temppath;

	this->set_modified();
}

void CasInode::update_mtime()
{
	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);

	this->attr.st_mtime = ts.tv_sec;
	this->attr.st_atime = ts.tv_sec;
	this->attr.st_ctime = ts.tv_sec;
}

void CasDirectory::flush()
{
	if (!this->modified) {
		return;
	}

	Directory directory;

	for (auto it = this->dentries.begin(); it != this->dentries.end(); it++) {
		CasInode *child_inode = it->second;
		if (child_inode) {
			child_inode->flush();

			if (S_ISDIR(child_inode->attr.st_mode)) {
				auto subdir_node = directory.add_directories();
				subdir_node->set_name(it->first);
				subdir_node->mutable_digest()->CopyFrom(child_inode->digest);
			} else if (S_ISREG(child_inode->attr.st_mode)) {
				auto file_node = directory.add_files();
				file_node->set_name(it->first);
				file_node->mutable_digest()->CopyFrom(child_inode->digest);
				file_node->set_is_executable((child_inode->attr.st_mode & 0100) != 0);
			} else if (S_ISLNK(child_inode->attr.st_mode)) {
				CasSymlink *symlink = (CasSymlink *) child_inode;
				auto symlink_node = directory.add_symlinks();
				symlink_node->set_name(it->first);
				symlink_node->set_target(symlink->target);
			} else if (S_ISFIFO(child_inode->attr.st_mode) ||
			           S_ISSOCK(child_inode->attr.st_mode)) {
				/* ignore FIFOs and sockets */
			} else {
				assert(0);
			}
		}
	}

	std::string temppath;
	int fd = fs.mktemp(temppath);
	if (!directory.SerializeToFileDescriptor(fd)) {
		::close(fd);
		throw std::runtime_error("Failed to serialize directory");
	}
	::close(fd);

	this->temppath = temppath;

	CasInode::flush();
}

void CasInode::flush()
{
	if (!this->modified) {
		return;
	}

	if (S_ISREG(this->attr.st_mode) || S_ISDIR(this->attr.st_mode)) {
		/* directory or regular file */

		/* Checksum file */

		int fd = open(this->temppath.c_str(), O_RDONLY);
		assert(fd >= 0);

		std::vector<char> buffer(BUFFER_SIZE);

		unsigned char hash[SHA256_DIGEST_LENGTH];
		SHA256_CTX sha256;
		SHA256_Init(&sha256);
		ssize_t bytes_read;
		ssize_t total_bytes_read = 0;
		while ((bytes_read = read(fd, &buffer[0], buffer.size())) > 0) {
			SHA256_Update(&sha256, &buffer[0], bytes_read);
			total_bytes_read += bytes_read;
		}
		SHA256_Final(hash, &sha256);

		std::stringstream ss;
		for (int i = 0; i < SHA256_DIGEST_LENGTH; i++) {
			ss << std::hex << std::setw(2) << std::setfill('0') << (int) hash[i];
		}

		this->digest.set_hash(ss.str());
		this->digest.set_size_bytes(total_bytes_read);

		/* Move it to the right place */

		mkdirat(fs.local_dfd, objdir(this->digest).c_str(), 0755);
		if (renameat(AT_FDCWD, this->temppath.c_str(), fs.local_dfd, objpath(this->digest).c_str()) < 0) {
			::close(fd);
			throw std::system_error(errno, std::generic_category());
		}

		try {
			/* Upload to CAS server */

			if (fs.opts.remote_url) {
				fs.client.upload(fd, this->digest);
			}

			this->modified = false;
		} catch (std::exception& e) {
			::close(fd);
			throw;
		}

		::close(fd);
	}
}

static void cas_fuse_init(void *userdata, struct fuse_conn_info *conn)
{
	/* Enable optimization: handle file open requests in the kernel.
	 * Unconditionally set this as it's currently required.
	 * libfuse will verify that the kernel supports it. */
	conn->want |= FUSE_CAP_NO_OPEN_SUPPORT;
	/* Atomic open and truncate is incompatible with FUSE_CAP_NO_OPEN_SUPPORT */
	conn->want &= ~FUSE_CAP_ATOMIC_O_TRUNC;

	/* Enable optimization: move data in kernelspace, if possible */
	if (conn->capable & FUSE_CAP_SPLICE_MOVE) {
		conn->want |= FUSE_CAP_SPLICE_MOVE;
	}
	if (conn->capable & FUSE_CAP_SPLICE_WRITE) {
		conn->want |= FUSE_CAP_SPLICE_WRITE;
	}
}

static int cas_fuse_do_lookup(fuse_req_t req, fuse_ino_t parent_id, const char *name, struct fuse_entry_param *e)
{
	try {
		CasDirectory *parent = CasDirectory::open_from_fuse(parent_id);

		/* Set high timeout as no changes can happen to the filesystem
		 * behind the kernel's back. */
		e->attr_timeout = ENTRY_TIMEOUT;
		e->entry_timeout = ENTRY_TIMEOUT;

		auto it = parent->dentries.find(name);
		if (it == parent->dentries.end() || !it->second) {
			/* No matching directory entry
			 * Returning entry with inode 0 instead of ENOENT
			 * enables negative dentry caching. */
			return 0;
		}

		CasInode *inode = it->second;

		memcpy(&e->attr, &inode->attr, sizeof(e->attr));
		e->ino = inode->attr.st_ino;

		return 0;
	} catch (std::exception& e) {
		return -EIO;
	}
}

static void cas_fuse_lookup(fuse_req_t req, fuse_ino_t parent_id, const char *name)
{
	struct fuse_entry_param e = {0};

	int ret = cas_fuse_do_lookup(req, parent_id, name, &e);

	if (ret == 0) {
		fuse_reply_entry(req, &e);
	} else {
		fuse_reply_err(req, -ret);
	}
}

static void cas_fuse_getattr(fuse_req_t req, fuse_ino_t ino, struct fuse_file_info *fi)
{
	CasInode *inode = CasInode::from_fuse(ino);

	fuse_reply_attr(req, &inode->attr, ENTRY_TIMEOUT);
}

static void cas_fuse_getxattr(fuse_req_t req, fuse_ino_t ino, const char *name, size_t size)
{
	CasInode *inode = CasInode::from_fuse(ino);

	if (std::string(name) == XATTR_CHECKSUM) {
		/* SHA256 checksum for regular files */

		if (S_ISREG(inode->attr.st_mode) && !inode->is_modified()) {
			/* Unmodified regular file, digest is available */

			std::string hash = inode->digest.hash();

			if (size == 0) {
				/* Return size of xattr value */
				fuse_reply_xattr(req, hash.size());
				return;
			}

			if (size < hash.size()) {
				/* Not enough space */
				fuse_reply_err(req, E2BIG);
				return;
			}

			fuse_reply_buf(req, hash.c_str(), hash.size());
		} else {
			fuse_reply_err(req, ENODATA);
		}
	} else {
		fuse_reply_err(req, ENODATA);
	}
}

static void cas_fuse_listxattr(fuse_req_t req, fuse_ino_t ino, size_t size)
{
	CasInode *inode = CasInode::from_fuse(ino);

	if (S_ISREG(inode->attr.st_mode) && !inode->is_modified()) {
		/* Unmodified regular file, digest is available */

		size_t list_size = strlen(XATTR_CHECKSUM) + 1;

		if (size == 0) {
			fuse_reply_xattr(req, list_size);
			return;
		}

		if (size < list_size) {
			/* Not enough space */
			fuse_reply_err(req, E2BIG);
			return;
		}

		fuse_reply_buf(req, XATTR_CHECKSUM, list_size);
	} else {
		/* No extended attributes */

		if (size == 0) {
			fuse_reply_xattr(req, 0);
			return;
		}

		fuse_reply_buf(req, NULL, 0);
	}
}

static void cas_fuse_setattr(fuse_req_t req, fuse_ino_t ino, struct stat *attr, int valid, struct fuse_file_info *fi)
{
	try {
		CasInode *inode = CasInode::from_fuse(ino);

		if (valid & FUSE_SET_ATTR_MODE) {
			/* Only the executable bit of regular files is stored,
			 * however, the full mode bits are kept in memory. */

			bool old_executable = (inode->attr.st_mode & 0100) != 0;
			bool new_executable = (attr->st_mode & 0100) != 0;

			inode->attr.st_mode &= ~07777;
			inode->attr.st_mode |= attr->st_mode & 07777;

			if (inode->parent && old_executable != new_executable) {
				/* The executable bit is stored in the parent directory */
				inode->parent->set_modified();
			}
		}

		if (valid & FUSE_SET_ATTR_SIZE) {
			if (S_ISREG(inode->attr.st_mode)) {
				CasFile *file = CasFile::open_from_fuse(ino);

				file->copy_on_write();

				if (ftruncate(file->fd, attr->st_size) < 0) {
					fuse_reply_err(req, errno);
					return;
				}
				file->attr.st_size = attr->st_size;
			}
		}

		if (valid & FUSE_SET_ATTR_MTIME_NOW) {
			inode->update_mtime();
		} else if (valid & FUSE_SET_ATTR_MTIME) {
			inode->attr.st_mtime = attr->st_mtime;
		}

		cas_fuse_getattr(req, ino, fi);
	} catch (std::exception& e) {
		fuse_reply_err(req, EIO);
	}
}

static void cas_fuse_fallocate(fuse_req_t req, fuse_ino_t ino, int mode, off_t offset, off_t len, struct fuse_file_info *fi)
{
	try {
		if (mode != 0) {
			fuse_reply_err(req, EOPNOTSUPP);
			return;
		}

		CasFile *file = CasFile::open_from_fuse(ino);

		file->copy_on_write();

		int err = posix_fallocate(file->fd, offset, len);
		if (err) {
			fuse_reply_err(req, err);
		}

		if (offset + len > file->attr.st_size) {
			file->attr.st_size = offset + len;
		}

		file->update_mtime();

		fuse_reply_err(req, 0);
	} catch (std::exception& e) {
		fuse_reply_err(req, EIO);
	}
}

static void cas_fuse_readlink(fuse_req_t req, fuse_ino_t ino)
{
	try {
		CasInode *inode = CasInode::from_fuse(ino);

		if (!S_ISLNK(inode->attr.st_mode)) {
			/* not a symlink */
			fuse_reply_err(req, EINVAL);
			return;
		}

		CasSymlink *symlink = (CasSymlink *) inode;

		fuse_reply_readlink(req, symlink->target.c_str());
	} catch (std::exception& e) {
		fuse_reply_err(req, EIO);
	}
}

OpenDirectory::OpenDirectory(CasDirectory *inode)
{
	this->offset = 0;
	this->iterator = inode->dentries.begin();
}

static void cas_fuse_opendir(fuse_req_t req, fuse_ino_t ino, struct fuse_file_info *fi)
{
	try {
		CasDirectory *inode = CasDirectory::open_from_fuse(ino);

		OpenDirectory *d = new OpenDirectory(inode);

		fi->fh = reinterpret_cast<uintptr_t>(d);
		fuse_reply_open(req, fi);
	} catch (std::exception& e) {
		fuse_reply_err(req, EIO);
	}
}

static void cas_fuse_readdir(fuse_req_t req, fuse_ino_t ino, size_t size, off_t offset, struct fuse_file_info *fi, bool plus)
{
	try {
		OpenDirectory *d = reinterpret_cast<OpenDirectory *>(fi->fh);
		char *p;
		size_t rem;

		CasDirectory *inode = CasDirectory::open_from_fuse(ino);
		if (!inode) {
			fuse_reply_err(req, errno);
			return;
		}

		std::vector<char> buf(size);

		if (offset != d->offset) {
			/* Offset changed since last readdir */

			if (offset == 0) {
				/* fast path */
				d->iterator = inode->dentries.begin();
			} else {
				/* O(n) */
				std::advance(d->iterator, offset - d->offset);
			}

			d->offset = offset;
		}

		p = &buf[0];
		rem = size;
		while (1) {
			size_t entsize;
			off_t nextoff;

			if (d->iterator == inode->dentries.end()) {
				break;
			}

			if (!d->iterator->second) {
				/* Deleted entry, continue */
				d->iterator++;
				d->offset++;
				continue;
			}

			const char *name = d->iterator->first.c_str();
			CasInode *child_inode = d->iterator->second;
			nextoff = d->offset + 1;

			if (plus) {
				struct fuse_entry_param e;

				int ret = cas_fuse_do_lookup(req, ino, name, &e);
				if (ret) {
					fuse_reply_err(req, -ret);
					return;
				}

				entsize = fuse_add_direntry_plus(req, p, rem,
								 name,
								 &e, nextoff);
			} else {
				entsize = fuse_add_direntry(req, p, rem,
							    name,
							    &child_inode->attr, nextoff);
			}
			if (entsize > rem) {
				/* Insufficient space in buffer for next entry */
				break;
			}

			p += entsize;
			rem -= entsize;

			d->iterator++;
			d->offset++;
		}

		fuse_reply_buf(req, &buf[0], size - rem);
	} catch (std::exception& e) {
		fuse_reply_err(req, EIO);
	}
}

static void cas_fuse_readdir(fuse_req_t req, fuse_ino_t ino, size_t size, off_t offset, struct fuse_file_info *fi)
{
	cas_fuse_readdir(req, ino, size, offset, fi, false);
}

static void cas_fuse_readdirplus(fuse_req_t req, fuse_ino_t ino, size_t size, off_t offset, struct fuse_file_info *fi)
{
	cas_fuse_readdir(req, ino, size, offset, fi, true);
}

static void cas_fuse_releasedir(fuse_req_t req, fuse_ino_t ino, struct fuse_file_info *fi)
{
	OpenDirectory *d = (OpenDirectory *) fi->fh;
	delete d;
	fuse_reply_err(req, 0);
}

static void cas_fuse_read(fuse_req_t req, fuse_ino_t ino, size_t size, off_t offset, struct fuse_file_info *fi)
{
	/* Read file block without moving data between kernel and userspace (splice) */

	try {
		struct fuse_bufvec buf = FUSE_BUFVEC_INIT(size);

		CasFile *inode = CasFile::open_from_fuse(ino);

		buf.buf[0].flags = fuse_buf_flags(FUSE_BUF_IS_FD | FUSE_BUF_FD_SEEK);
		buf.buf[0].fd = inode->fd;
		buf.buf[0].pos = offset;

		fuse_reply_data(req, &buf, FUSE_BUF_SPLICE_MOVE);
	} catch (std::exception& e) {
		fuse_reply_err(req, EIO);
	}
}

static void cas_fuse_write_buf(fuse_req_t req, fuse_ino_t ino, struct fuse_bufvec *in_buf, off_t offset, struct fuse_file_info *fi)
{
	/* Write file block without moving data between kernel and userspace (splice) */

	try {
		struct fuse_bufvec out_buf = FUSE_BUFVEC_INIT(fuse_buf_size(in_buf));

		CasFile *inode = CasFile::open_from_fuse(ino);

		inode->copy_on_write();

		out_buf.buf[0].flags = fuse_buf_flags(FUSE_BUF_IS_FD | FUSE_BUF_FD_SEEK);
		out_buf.buf[0].fd = inode->fd;
		out_buf.buf[0].pos = offset;

		ssize_t res = fuse_buf_copy(&out_buf, in_buf, fuse_buf_copy_flags(0));
		if (res < 0) {
			fuse_reply_err(req, -res);
		} else {
			if (offset + res > inode->attr.st_size) {
				/* Kernel implicitly extends file, if needed.
				 * Keep track on our side as well. */
				inode->attr.st_size = offset + res;
			}

			inode->update_mtime();
			fuse_reply_write(req, (size_t) res);
		}
	} catch (std::exception& e) {
		fuse_reply_err(req, EIO);
	}
}

static void cas_fuse_create(fuse_req_t req, fuse_ino_t parent_id, const char *name, mode_t mode, struct fuse_file_info *fi)
{
	/* Create new file */

	try {
		CasDirectory *parent = CasDirectory::open_from_fuse(parent_id);

		auto it = parent->dentries.find(name);
		if (it != parent->dentries.end() && it->second) {
			fuse_reply_err(req, EEXIST);
			return;
		}

		CasInode *inode = new CasFile();

		/* Store specified mode */
		inode->attr.st_mode &= ~07777;
		inode->attr.st_mode |= mode & 07777;

		parent->update_mtime();
		parent->set_modified();
		parent->dentries[name] = inode;

		struct fuse_entry_param e = {0};
		int ret = cas_fuse_do_lookup(req, parent_id, name, &e);

		if (ret == 0) {
			fuse_reply_create(req, &e, fi);
		} else {
			fuse_reply_err(req, -ret);
		}
	} catch (std::exception& e) {
		fuse_reply_err(req, EIO);
	}
}

static void cas_fuse_mkdir(fuse_req_t req, fuse_ino_t parent_id, const char *name, mode_t mode)
{
	try {
		CasDirectory *parent = CasDirectory::open_from_fuse(parent_id);

		auto it = parent->dentries.find(name);
		if (it != parent->dentries.end() && it->second) {
			fuse_reply_err(req, EEXIST);
			return;
		}

		CasInode *inode = new CasDirectory();

		/* Store specified mode */
		inode->attr.st_mode &= ~07777;
		inode->attr.st_mode |= mode & 07777;

		parent->update_mtime();
		parent->set_modified();
		parent->dentries[name] = inode;

		struct fuse_entry_param e = {0};
		int ret = cas_fuse_do_lookup(req, parent_id, name, &e);

		if (ret == 0) {
			fuse_reply_entry(req, &e);
		} else {
			fuse_reply_err(req, -ret);
		}
	} catch (std::exception& e) {
		fuse_reply_err(req, EIO);
	}
}

static void cas_fuse_symlink(fuse_req_t req, const char *link, fuse_ino_t parent_id, const char *name)
{
	try {
		CasDirectory *parent = CasDirectory::open_from_fuse(parent_id);

		auto it = parent->dentries.find(name);
		if (it != parent->dentries.end() && it->second) {
			fuse_reply_err(req, EEXIST);
			return;
		}

		CasSymlink *inode = new CasSymlink(link);

		inode->update_mtime();
		parent->update_mtime();
		parent->set_modified();
		inode->set_modified();

		parent->dentries[name] = inode;

		struct fuse_entry_param e = {0};
		int ret = cas_fuse_do_lookup(req, parent_id, name, &e);

		if (ret == 0) {
			fuse_reply_entry(req, &e);
		} else {
			fuse_reply_err(req, -ret);
		}
	} catch (std::exception& e) {
		fuse_reply_err(req, EIO);
	}
}

static void cas_fuse_mknod(fuse_req_t req, fuse_ino_t parent_id, const char *name, mode_t mode, dev_t rdev)
{
	try {
		CasDirectory *parent = CasDirectory::open_from_fuse(parent_id);

		auto it = parent->dentries.find(name);
		if (it != parent->dentries.end() && it->second) {
			fuse_reply_err(req, EEXIST);
			return;
		}

		if (!S_ISFIFO(mode) && !S_ISSOCK(mode)) {
			/* Device node creation is not supported */
			fuse_reply_err(req, EPERM);
			return;
		}

		CasInode *inode = new CasInode(nullptr);

		/* Store specified mode, this includes the file type */
		inode->attr.st_mode = mode;

		parent->update_mtime();
		parent->set_modified();
		parent->dentries[name] = inode;

		struct fuse_entry_param e = {0};
		int ret = cas_fuse_do_lookup(req, parent_id, name, &e);

		if (ret == 0) {
			fuse_reply_entry(req, &e);
		} else {
			fuse_reply_err(req, -ret);
		}
	} catch (std::exception& e) {
		fuse_reply_err(req, EIO);
	}
}

static void cas_fuse_unlink(fuse_req_t req, fuse_ino_t parent_id, const char *name)
{
	try {
		CasDirectory *parent = CasDirectory::open_from_fuse(parent_id);

		auto it = parent->dentries.find(name);
		if (it == parent->dentries.end() || !it->second) {
			fuse_reply_err(req, ENOENT);
			return;
		}

		if (S_ISDIR(it->second->attr.st_mode)) {
			fuse_reply_err(req, EISDIR);
			return;
		}

		it->second->attr.st_nlink--;
		it->second = nullptr;
		parent->update_mtime();
		parent->set_modified();
		fuse_reply_err(req, 0);
	} catch (std::exception& e) {
		fuse_reply_err(req, EIO);
	}
}

static void cas_fuse_rmdir(fuse_req_t req, fuse_ino_t parent_id, const char *name)
{
	try {
		CasDirectory *parent = CasDirectory::open_from_fuse(parent_id);

		auto it = parent->dentries.find(name);
		if (it == parent->dentries.end() || !it->second) {
			fuse_reply_err(req, ENOENT);
			return;
		}

		if (!S_ISDIR(it->second->attr.st_mode)) {
			fuse_reply_err(req, ENOTDIR);
			return;
		}

		CasDirectory *dir = CasDirectory::open_from_fuse(it->second->attr.st_ino);

		for (auto child_it = dir->dentries.begin(); child_it != dir->dentries.end(); child_it++) {
			if (child_it->second) {
				fuse_reply_err(req, ENOTEMPTY);
				return;
			}
		}

		it->second->attr.st_nlink--;
		it->second = nullptr;
		parent->update_mtime();
		parent->set_modified();
		fuse_reply_err(req, 0);
	} catch (std::exception& e) {
		fuse_reply_err(req, EIO);
	}
}

static void cas_fuse_rename(fuse_req_t req, fuse_ino_t parent_id, const char *name, fuse_ino_t newparent_id, const char *newname, unsigned int flags)
{
	if (flags != 0) {
		fuse_reply_err(req, EINVAL);
		return;
	}

	try {
		CasDirectory *parent = CasDirectory::open_from_fuse(parent_id);
		CasDirectory *newparent = CasDirectory::open_from_fuse(newparent_id);

		auto it = parent->dentries.find(name);
		if (it == parent->dentries.end() || !it->second) {
			fuse_reply_err(req, ENOENT);
			return;
		}

		auto newit = newparent->dentries.find(newname);

		if (newit == newparent->dentries.end() || !newit->second) {
			/* No entry with the new name yet, add it */
			newparent->dentries[newname] = it->second;
		} else {
			/* New name already exists, replace it */
			newit->second->attr.st_nlink--;
			newit->second = it->second;
		}
		it->second->parent = nullptr;
		it->second = nullptr;
		parent->set_modified();
		newparent->set_modified();
		fuse_reply_err(req, 0);
	} catch (std::exception& e) {
		fuse_reply_err(req, EIO);
	}
}

static void cas_fuse_link(fuse_req_t req, fuse_ino_t ino, fuse_ino_t newparent_id, const char *newname)
{
	try {
		CasInode *inode = CasInode::from_fuse(ino);
		if (S_ISDIR(inode->attr.st_mode)) {
			/* Can't hardlink directories */
			fuse_reply_err(req, EPERM);
			return;
		}

		CasDirectory *newparent = CasDirectory::open_from_fuse(newparent_id);

		auto newit = newparent->dentries.find(newname);

		if (newit == newparent->dentries.end() || !newit->second) {
			/* No entry with the new name yet, add it */
			newparent->dentries[newname] = inode;
		} else {
			/* New name already exists, do not replace it */
			fuse_reply_err(req, EEXIST);
			return;
		}
		newparent->set_modified();

		inode->attr.st_nlink++;

		struct fuse_entry_param e = {0};
		int ret = cas_fuse_do_lookup(req, newparent_id, newname, &e);

		if (ret == 0) {
			fuse_reply_entry(req, &e);
		} else {
			fuse_reply_err(req, -ret);
		}
	} catch (std::exception& e) {
		fuse_reply_err(req, EIO);
	}
}

static void cas_fuse_open(fuse_req_t req, fuse_ino_t ino, struct fuse_file_info *fi)
{
	/* We let the kernel handle file open requests to reduce round-trips */
	fuse_reply_err(req, ENOSYS);
}

static struct fuse_lowlevel_ops fs_oper = {
	.init		= cas_fuse_init,
	.destroy	= NULL,
	.lookup		= cas_fuse_lookup,
	.forget		= NULL,
	.getattr	= cas_fuse_getattr,
	.setattr	= cas_fuse_setattr,
	.readlink	= cas_fuse_readlink,
	.mknod		= cas_fuse_mknod,
	.mkdir		= cas_fuse_mkdir,
	.unlink		= cas_fuse_unlink,
	.rmdir		= cas_fuse_rmdir,
	.symlink	= cas_fuse_symlink,
	.rename		= cas_fuse_rename,
	.link		= cas_fuse_link,
	.open		= cas_fuse_open,
	.read		= cas_fuse_read,
	.write		= NULL,
	.flush		= NULL,
	.release	= NULL,
	.fsync		= NULL,
	.opendir	= cas_fuse_opendir,
	.readdir	= cas_fuse_readdir,
	.releasedir	= cas_fuse_releasedir,
	.fsyncdir	= NULL,
	.statfs		= NULL,
	.setxattr	= NULL,
	.getxattr	= cas_fuse_getxattr,
	.listxattr	= cas_fuse_listxattr,
	.removexattr	= NULL,
	.access		= NULL,
	.create		= cas_fuse_create,
	.getlk		= NULL,
	.setlk		= NULL,
	.bmap		= NULL,
	.ioctl		= NULL,
	.poll		= NULL,
	.write_buf	= cas_fuse_write_buf,
	.retrieve_reply	= NULL,
	.forget_multi	= NULL,
	.flock		= NULL,
	.fallocate	= cas_fuse_fallocate,
	.readdirplus	= cas_fuse_readdirplus,
};

static void usage(const char *name) {
	std::cerr << "usage: " << name << " [OPTIONS] MOUNTPOINT COMMAND [ARGS...]\n";
	std::cerr << "    --local=PATH           Local CAS cache directory\n";
	std::cerr << "    --input-digest=PATH    Path to input directory digest\n";
	std::cerr << "    --output-digest=PATH   Path to output directory digest\n";
	std::cerr << "    --chdir=PATH           Change directory to PATH\n";
	std::cerr << "    --remote=URL           URL for remote CAS server\n";
	std::cerr << "    --server-cert=PATH     Public server certificate for TLS (PEM-encoded)\n";
	std::cerr << "    --client-key=PATH      Private client key for TLS (PEM-encoded)\n";
	std::cerr << "    --client-cert=PATH     Public client certificate for TLS (PEM-encoded)\n";
	std::cerr << "    --prefetch             Prefetch input directory from CAS server\n";
	std::cerr << "    --clearenv             Clear environment variables for COMMAND\n";
	std::cerr << "    --setenv VAR VALUE     Set an environment variable for COMMAND\n";
	std::cerr << "    --unsetenv VAR         Unset an environment variable for COMMAND\n";
}

static void child_handler(int sig)
{
	siginfo_t si = {0};
	if (waitid(P_PID, fs.cmd_child_pid, &si, WEXITED | WNOHANG) == 0) {
		if (si.si_pid == fs.cmd_child_pid) {
			fs.cmd_child_status = si.si_status;
			fuse_session_exit(fs.fuse);
		}
	}
}

static void ignore_signal(int sig)
{
	struct sigaction act = {0};
	act.sa_handler = SIG_IGN;
	sigemptyset(&act.sa_mask);
	sigaction(sig, &act, NULL);
}

static void install_signal_handlers()
{
	/* do not terminate before command child process terminates */
	ignore_signal(SIGHUP);
	ignore_signal(SIGINT);
	ignore_signal(SIGTERM);
	ignore_signal(SIGPIPE);

	struct sigaction act = {0};
	act.sa_handler = child_handler;
	sigemptyset(&act.sa_mask);
	act.sa_flags = SA_NOCLDSTOP;
	sigaction(SIGCHLD, &act, NULL);
}

int main(int argc, char *argv[])
{
	int command_argc = 0;
	char **command_argv = NULL;

	char *prgname = argv[0];
	argv++;
	argc--;

	while (argc > 0) {
		const char *arg = argv[0];
		const char *assign = strchr(arg, '=');
		if (arg[0] == '-' && arg[1] == '-') {
			arg += 2;
			if (assign) {
				int key_len = assign - arg;
				const char *value = assign + 1;
				if (strncmp(arg, "local", key_len) == 0) {
					fs.opts.local_path = value;
				} else if (strncmp(arg, "input-digest", key_len) == 0) {
					fs.opts.input_digest = value;
				} else if (strncmp(arg, "output-digest", key_len) == 0) {
					fs.opts.output_digest = value;
				} else if (strncmp(arg, "chdir", key_len) == 0) {
					fs.opts.chdir = value;
				} else if (strncmp(arg, "remote", key_len) == 0) {
					fs.opts.remote_url = value;
				} else if (strncmp(arg, "server-cert", key_len) == 0) {
					fs.opts.server_cert = value;
				} else if (strncmp(arg, "client-key", key_len) == 0) {
					fs.opts.client_key = value;
				} else if (strncmp(arg, "client-cert", key_len) == 0) {
					fs.opts.client_cert = value;
				} else {
					std::cerr << "Invalid option " << argv[0] << "\n";
					usage(prgname);
					return 1;
				}
			} else {
				if (strcmp(arg, "help") == 0) {
					usage(prgname);
					return 0;
				} else if (strcmp(arg, "prefetch") == 0) {
					fs.opts.prefetch = 1;
				} else if (strcmp(arg, "clearenv") == 0) {
					if (!fs.env_argv.empty()) {
						std::cerr << "--clearenv must not follow --setenv or --unsetenv\n";
						return 1;
					}
					fs.env_argv.push_back(argv[0]);
				} else if (strcmp(arg, "setenv") == 0) {
					if (argc < 3) {
						std::cerr << "Missing argument option for --" << argv[0] << "\n";
						usage(prgname);
						return 1;
					}

					fs.env_argv.push_back(argv[0]);
					fs.env_argv.push_back(argv[1]);
					fs.env_argv.push_back(argv[2]);
					argv += 2;
					argc -= 2;
				} else if (strcmp(arg, "unsetenv") == 0) {
					if (argc < 2) {
						std::cerr << "Missing argument option for --" << argv[0] << "\n";
						usage(prgname);
						return 1;
					}

					fs.env_argv.push_back(argv[0]);
					fs.env_argv.push_back(argv[1]);
					argv++;
					argc--;
				} else {
					std::cerr << "Invalid option " << argv[0] << "\n";
					usage(prgname);
					return 1;
				}
			}
		} else {
			fs.opts.mountpoint = arg;
		}
		argv++;
		argc--;

		if (fs.opts.mountpoint) {
			/* Mountpoint is last non-command argument */
			command_argc = argc;
			command_argv = argv;
			argc = 0;
			argv = NULL;
		}
	}

	fs.uid = getuid();
	fs.gid = getgid();

	if (!fs.opts.mountpoint) {
		std::cerr << "Mountpoint is missing\n\n";
		usage(prgname);
		return 1;
	}

	if (!fs.opts.local_path) {
		std::cerr << "Local CAS cache directory is missing\n\n";
		usage(prgname);
		return 1;
	}
	fs.local_dfd = open(fs.opts.local_path, O_RDONLY | O_DIRECTORY);
	if (fs.local_dfd < 0) {
		std::cerr << "Failed to open " << fs.opts.local_path << ": " << std::strerror(errno) << "\n";
		return 1;
	}
	mkdirat(fs.local_dfd, "objects", 0755);
	mkdirat(fs.local_dfd, "tmp", 0755);

	if (fs.opts.remote_url) {
		fs.client.init(fs.opts.remote_url, fs.opts.server_cert, fs.opts.client_key, fs.opts.client_cert);
	}

	/* read digest of sandbox root directory (input tree) */
	Digest digest;
	if (fs.opts.input_digest) {
		int fd = open(fs.opts.input_digest, O_RDONLY);
		if (fd < 0) {
			std::cerr << "Failed to open " << fs.opts.input_digest << ": " << std::strerror(errno) << "\n";
			return 1;
		}
		if (!digest.ParseFromFileDescriptor(fd) || digest.hash().empty()) {
			::close(fd);
			std::cerr << "Failed to parse input digest\n";
			return 1;
		}
		::close(fd);

		fs.root_inode = new CasDirectory(nullptr, digest);

		if (fs.opts.prefetch) {
			if (!fs.opts.remote_url) {
				std::cerr << "--prefetch requires --remote\n";
				return 1;
			}

			fs.prefetch();
		}
	} else {
		/* empty input tree */
		fs.root_inode = new CasDirectory();
	}

	fs.root_inode->attr.st_ino = FUSE_ROOT_ID;

	struct fuse_args args = FUSE_ARGS_INIT(1, &prgname);

	fs.fuse = fuse_session_new(&args, &fs_oper, sizeof(fs_oper), &fs);
	if (fs.fuse == nullptr) {
		return 1;
	}

	bool run_command = (command_argc > 0);

	if (!run_command) {
		/* No command specified, mount but don't execute bubblewrap */

		if (fuse_set_signal_handlers(fs.fuse) != 0) {
			return 1;
		}

		if (fuse_session_mount(fs.fuse, fs.opts.mountpoint) != 0) {
			return 1;
		}
	} else {
		/* Command specified */

		install_signal_handlers();

		if (fuse_session_mount(fs.fuse, fs.opts.mountpoint) != 0) {
			return 1;
		}

		fs.cmd_child_pid = fork();
		if (fs.cmd_child_pid < 0) {
			return 1;
		}

		if (fs.cmd_child_pid == 0) {
			/* command child */

			try {
				execute_command(command_argc, command_argv, fs.opts.mountpoint, fs.opts.chdir, fs.env_argv);
			} catch (std::exception& e) {
				std::cerr << "Failed to execute command: " << e.what() << "\n";
			}

			_exit(1);
		}
	}

	int output_digest_fd = -1;

	if (fs.opts.output_digest) {
		output_digest_fd = creat(fs.opts.output_digest, 0666);
		if (output_digest_fd < 0) {
			std::cerr << "Failed to open " << fs.opts.output_digest << ": " << std::strerror(errno) << "\n";
			return 1;
		}
	}

	fuse_session_loop(fs.fuse);

	try {
		/* Recalculate digests and push changes to CAS */
		fs.root_inode->flush();
	} catch (std::exception& e) {
		::close(output_digest_fd);
		std::cerr << "Failed to flush changes to CAS: " << e.what() << "\n";
		return 1;
	}

	if (fs.opts.output_digest) {
		/* Output new digest of root directory */
		if (!fs.root_inode->digest.SerializeToFileDescriptor(output_digest_fd)) {
			::close(output_digest_fd);
			std::cerr << "Failed to serialize output digest\n";
			return 1;
		}
		::close(output_digest_fd);
	}

	fuse_session_unmount(fs.fuse);
	fuse_session_destroy(fs.fuse);
	fuse_opt_free_args(&args);

	return fs.cmd_child_status;
}
