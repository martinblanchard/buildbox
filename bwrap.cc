/*
 * Copyright 2018 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <errno.h>
#include <unistd.h>

#include <vector>

#include "buildbox.h"

extern char **environ;

void execute_command(int argc, char *argv[], const char *root, const char *working_directory, std::vector<const char *> env_argv)
{
	std::vector<const char *> bwrap_argv;
	bwrap_argv.push_back("bwrap");

	/* Create a new pid namespace, this also ensures that any subprocesses
	 * are cleaned up when the bwrap process exits.
	 */
	bwrap_argv.push_back("--unshare-pid");

	/* Ensure subprocesses are cleaned up when the bwrap parent dies. */
	bwrap_argv.push_back("--die-with-parent");

	/* Mount FUSE as sandbox rootfs */
	bwrap_argv.push_back("--bind");
	bwrap_argv.push_back(root);
	bwrap_argv.push_back("/");

	/* Disable network access */
	bwrap_argv.push_back("--unshare-net");
	bwrap_argv.push_back("--unshare-uts");
	bwrap_argv.push_back("--hostname");
	bwrap_argv.push_back("buildbox");
	bwrap_argv.push_back("--unshare-ipc");

	if (working_directory) {
		bwrap_argv.push_back("--chdir");
		bwrap_argv.push_back(working_directory);
	}

	/* Set environment for command process */
	std::vector<std::string> environ_keys;
	bool first = true;
	for (const char *env_arg : env_argv) {
		/* --clearenv must be first environment option */
		if (first && strcmp(env_arg, "--clearenv") == 0) {
			for (char **envp = environ; *envp; envp++) {
				const char *assign = strchr(*envp, '=');
				assert(assign);
				environ_keys.push_back(std::string(*envp, assign - *envp));
			}
			for (std::string &key : environ_keys) {
				bwrap_argv.push_back("--unsetenv");
				bwrap_argv.push_back(key.c_str());
			}
		} else {
			/* --setenv and --unsetenv syntax is the same for buildbox and bwrap */
			bwrap_argv.push_back(env_arg);
		}
		first = false;
	}

	/* Give it a proc and tmpfs */
	bwrap_argv.push_back("--proc");
	bwrap_argv.push_back("/proc");
	bwrap_argv.push_back("--tmpfs");
	bwrap_argv.push_back("/tmp");

	static const char *devices[] = {
		"/dev/full",
		"/dev/null",
		"/dev/urandom",
		"/dev/random",
		"/dev/zero",
		NULL
	};

	for (const char **device = devices; *device; device++) {
		bwrap_argv.push_back("--dev-bind");
		bwrap_argv.push_back(*device);
		bwrap_argv.push_back(*device);
	}

	bwrap_argv.push_back("--unshare-user");

	/* append command */
	for (int i = 0; i < argc; i++) {
		bwrap_argv.push_back(argv[i]);
	}

	bwrap_argv.push_back(nullptr);

	execvp("bwrap", const_cast<char **>(&bwrap_argv[0]));

	/* On success, execvp() does not return */
	throw std::system_error(errno, std::generic_category());
}
