#!/bin/bash

. $(dirname "$0")/fuse.sh

if [ -z "$ECHO" ] ; then
	echo "Missing echo path" >&2
	exit 1
fi

if [ "$#" -lt 1 ] ; then
	echo "Missing test name" >&2
	exit 1
fi
TEST="$1"
shift

case "$TEST" in
inherit_env)
	SETPATH=
	;;
set_path)
	SETPATH=1
	;;
*)
	echo Invalid test "$TEST" >&2
	exit 1
	;;
esac

# Store the statically linked echo binary in CAS via FUSE
start_buildbox
cp "$ECHO" "$ROOT/echo"
stop_buildbox
# Use output tree as new input tree
mv "$TMPDIR/out" "$TMPDIR/in"

ENV_ARGS=
if [ -n "$SETPATH" ] ; then
	ENV_ARGS="--clearenv --setenv PATH /"
	CMD="echo"
else
	CMD="/echo"
fi

"$BUILDBOX" --local="$TMPDIR/cas" --input-digest="$TMPDIR/in" --output-digest="$TMPDIR/out" $ENV_ARGS "$ROOT" "$CMD" hello, world > "$TMPDIR/echo.out"
echo hello, world > "$TMPDIR/echo.ref"

if ! diff -u "$TMPDIR/echo.ref" "$TMPDIR/echo.out" ; then
	echo Echo output does not match reference >&2
	exit 1
fi
